/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;          
import java.awt.event.*;  

public class QuestionCard extends JPanel implements ActionListener {

	private JPanel cardDeck;
	private JPanel numberPanel;
	private JPanel picturePanel;
	private JPanel questionPanel;
	private JPanel radiobuttonPanel;
	private JPanel nextPanel;
	private JLabel questionNumber;
	private JLabel question;
	private JLabel frame;
	private JButton next;
	private JRadioButton optionOne, optionTwo, optionThree;
	private ButtonGroup buttonGroup;
	private ImageIcon picture;
	private String choice;
	private String rightChoice;
	private int rc;
	private Border border;
	private Box radiobuttonBox;
	
	public QuestionCard (JPanel panel, String qstnNo, String qstn, String optOne, String optTwo, String optThree, int rightOpt, String source){
		
		/* Kysymyskortin määrittely */
		cardDeck = panel;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(500,500));
		
		numberPanel = new JPanel();
		numberPanel.setPreferredSize(new Dimension(500,25));
		
		picturePanel = new JPanel();
		picturePanel.setPreferredSize(new Dimension(500,265));
		picturePanel.setBackground(Color.lightGray);
		
		questionPanel = new JPanel();
		questionPanel.setPreferredSize(new Dimension(500,25));
		
		radiobuttonPanel = new JPanel();
		radiobuttonPanel.setPreferredSize(new Dimension(200,75));
		radiobuttonPanel.setLayout(new GridLayout());
		
		nextPanel = new JPanel();
		nextPanel.setPreferredSize(new Dimension(500,30));
		
		next = new JButton("Seuraava");
		
		rc = rightOpt;
		
		question = new JLabel(qstn);
		question.setFont(new Font("Arial", Font.BOLD, 15));
		
		questionNumber = new JLabel(qstnNo);
		questionNumber.setFont(new Font("Arial", Font.BOLD, 15));
		
		numberPanel.add(questionNumber);
		questionPanel.add(question);
		
		picture = new ImageIcon(source);
		frame = new JLabel(picture);
		picturePanel.add(frame);
		
		border = BorderFactory.createCompoundBorder(
				 BorderFactory.createRaisedBevelBorder(),
				 BorderFactory.createLoweredBevelBorder());
		
		frame.setBorder(border);
		
		choice = new String();
		choice = null;
		
		optionOne = new JRadioButton(optOne);
		optionTwo = new JRadioButton(optTwo);
		optionThree = new JRadioButton(optThree);
		
		optionOne.addActionListener(this);
		optionOne.setActionCommand("1");
		optionTwo.addActionListener(this);
		optionTwo .setActionCommand("2");
		optionThree.addActionListener(this);
		optionThree.setActionCommand("3");
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(optionOne);
		buttonGroup.add(optionTwo);
		buttonGroup.add(optionThree);
	
		radiobuttonBox = Box.createVerticalBox();
		radiobuttonBox.add(optionOne);
		radiobuttonBox.add(optionTwo);
		radiobuttonBox.add(optionThree);
		
		/* ActionListenerit*/
		next.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				if(choice.equals(checkOption())){
					GUI.player.addPoint();
				}
				
				if (GUI.ans.getCounter() == 0){
					GUI.ans.setAnsOne(choice);
				} else if (GUI.ans.getCounter() == 1){
					GUI.ans.setAnsTwo(choice);
				} else if (GUI.ans.getCounter() == 2){
					GUI.ans.setAnsThree(choice);
				} else if (GUI.ans.getCounter() == 3){
					GUI.ans.setAnsFour(choice);
				} else if (GUI.ans.getCounter() == 4){
					GUI.ans.setAnsFive(choice);
				} else if (GUI.ans.getCounter() == 5){
					GUI.ans.setAnsSix(choice);
				} else if (GUI.ans.getCounter() == 6){
					GUI.ans.setAnsSeven(choice);
				} else if (GUI.ans.getCounter() == 7){
					GUI.ans.setAnsEight(choice);
				} else if (GUI.ans.getCounter() == 8){
					GUI.ans.setAnsNine(choice);
				} else if (GUI.ans.getCounter() == 9){
					GUI.ans.setAnsTen(choice);
				}
			
				GUI.ans.counterIncrease();
				buttonGroup.clearSelection();
				next.setEnabled(false);
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
				 cardLayout.next(cardDeck);
				 
				
		}});
		
		next.setEnabled(false);
		
		add(numberPanel);
		add(picturePanel);
		add(questionPanel);
		radiobuttonPanel.add(radiobuttonBox);
		radiobuttonPanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
		
		add(radiobuttonPanel);
		
		nextPanel.add(next);
		add(nextPanel);
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		
		next.setEnabled(true);
		choice = e.getActionCommand();
	}

	
	public String checkOption()
	{
		if( rc == 1){
			rightChoice = "1";
		} else if (rc == 2){
			rightChoice = "2";
		} else {
			rightChoice = "3";
		}
		
		return rightChoice;
	}
	
}
