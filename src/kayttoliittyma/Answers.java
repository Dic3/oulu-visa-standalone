/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

public class Answers {

	private String answerOne, answerTwo, answerThree, answerFour, answerFive,
				   answerSix, answerSeven, answerEight, answerNine, answerTen;
	private int answerCounter = 0;

	
	/* Setterit */
	public void setAnsOne(String ansOne)
	{
		answerOne = ansOne;

		if (answerOne == "1"){
			answerOne = "a";
		} else if (answerOne == "2"){
			answerOne = "b";
		} else {
			answerOne = "c";
		}
	}

	public void setAnsTwo(String ansTwo){

		answerTwo = ansTwo;

		if (answerTwo == "1"){
			answerTwo = "a";
		} else if (answerTwo == "2"){
			answerTwo = "b";
		} else {
			answerTwo = "c";
		}
	}

	public void setAnsThree(String ansThree){

		answerThree = ansThree;

		if (answerThree == "1"){
			answerThree = "a";
		} else if (answerThree == "2"){
			answerThree = "b";
		} else {
			answerThree = "c";
		}
	}

	public void setAnsFour(String ansFour){

		answerFour = ansFour;

		if (answerFour == "1"){
			answerFour = "a";
		} else if (answerFour == "2"){
			answerFour = "b";
		} else {
			answerFour = "c";
		}
	}

	public void setAnsFive(String ansFive){

		answerFive = ansFive;

		if (answerFive == "1"){
			answerFive = "a";
		} else if (answerFive == "2"){
			answerFive = "b";
		} else {
			answerFive = "c";
		}
	}

	public void setAnsSix(String ansSix){

		answerSix = ansSix;

		if (answerSix == "1"){
			answerSix = "a";
		} else if (answerSix == "2"){
			answerSix = "b";
		} else {
			answerSix = "c";
		}
	}

	public void setAnsSeven(String ansSeven){

		answerSeven = ansSeven;

		if (answerSeven == "1"){
			answerSeven = "a";
		} else if (answerSeven == "2"){
			answerSeven= "b";
		} else {
			answerSeven = "c";
		}
	}

	public void setAnsEight(String ansEight){

		answerEight = ansEight;

		if (answerEight == "1"){
			answerEight = "a";
		} else if (answerEight == "2"){
			answerEight = "b";
		} else {
			answerEight = "c";
		}
	}

	public void setAnsNine(String ansNine){

		answerNine = ansNine;

		if (answerNine == "1"){
			answerNine = "a";
		} else if (answerNine == "2"){
			answerNine = "b";
		} else {
			answerNine = "c";
		}
	}

	public void setAnsTen(String ansTen){

		answerTen = ansTen;

		if (answerTen == "1"){
			answerTen = "a";
		} else if (answerTen == "2"){
			answerTen = "b";
		} else {
			answerTen = "c";
		}
	}

	/* Getterit */
	public String getAnsOne()
	{
		return answerOne;
	}

	public String getAnsTwo()
	{
		return answerTwo;
	}

	public String getAnsThree()
	{
		return answerThree;
	}
	
	public String getAnsFour()
	{
		return answerFour;
	}
	
	public String getAnsFive()
	{
		return answerFive;
	}
	
	public String getAnsSix()
	{
		return answerSix;
	}
	
	public String getAnsSeven()
	{
		return answerSeven;
	}
	
	public String getAnsEight()
	{
		return answerEight;
	}
	
	public String getAnsNine()
	{
		return answerNine;
	}

	public String getAnsTen()
	{
		return answerTen;
	}

	public void counterIncrease()
	{
		answerCounter++;
	}

	public int getCounter()
	{
		return answerCounter;
	}

	/* Laskurin nollaus */
	public void resetCounter()
	{
		answerCounter = 0;
	}
}
