/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;

import java.awt.*;          
import java.awt.event.*;  

public class ResultCard extends JPanel {

	private JPanel cardDeck;
	private JButton startMenu;
	private JButton answers;
	private JButton showResults;
	private JLabel playerName;
	private JLabel playerPoints;
	private JLabel title;
	private JLabel textOne, textTwo;
	private Box horizontalBox;
	
	public ResultCard (JPanel panel){
		
		/* Tuloskortin määrittely */
		cardDeck = panel;
		setPreferredSize(new Dimension(500,500));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		title = new JLabel("Tulokset");
		title.setFont(new Font("Bookman Old Style", Font.BOLD, 25));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		title.setBorder(BorderFactory.createEmptyBorder(5 ,0,20 ,0));
		
		textOne = new JLabel("Kiitos vastauksistasi!");
		textOne.setFont(new Font("Bookman Old Style", Font.BOLD, 20));
		textOne.setAlignmentX(Component.CENTER_ALIGNMENT);
		textOne.setBorder(BorderFactory.createEmptyBorder(140 ,0 ,0 ,0));
		
		textTwo =  new JLabel("Sirry tuloksiin ja katso miten pärjäsit.");
		textTwo.setFont(new Font("Bookman Old Style", Font.BOLD, 20));
		textTwo.setAlignmentX(Component.CENTER_ALIGNMENT);
		textTwo.setBorder(BorderFactory.createEmptyBorder(0 ,0 ,20 ,0));
		
		showResults = new JButton("Tulokset");
		showResults.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		startMenu = new JButton("Alkuun");
		startMenu.setPreferredSize(new Dimension(140, 0));
		answers = new JButton("Oikeat vastaukset");
		answers.setPreferredSize(new Dimension(140, 0));
		horizontalBox = Box.createHorizontalBox();
		
		/* ActionListenerit*/
		showResults.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				setBackground(Color.lightGray);
				
				playerName = new JLabel(greeting() + " " + GUI.player.getName() + "!");
				playerName.setFont(new Font("Bookman Old Style", Font.BOLD, 25));
				playerName.setAlignmentX(Component.CENTER_ALIGNMENT);
				playerName.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
				
				playerPoints = new JLabel("Sait oikein " + GUI.player.getPoints() + "/10 kysymystä.");
				playerPoints.setFont(new Font("Bookman Old Style", Font.BOLD, 25));
				playerPoints.setAlignmentX(Component.CENTER_ALIGNMENT);
				playerPoints.setBorder(BorderFactory.createEmptyBorder(0,0,270,0));
				
				add(title);
				add(playerName);
				add(playerPoints);
				add(horizontalBox);
		
				showResults.setVisible(false);
				textOne.setVisible(false);
				textTwo.setVisible(false);
				horizontalBox.setVisible(true);
				
				validate();
		}});
	
		startMenu.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				setBackground(null);
				
				showResults.setVisible(true);
				textOne.setVisible(true);
				textTwo.setVisible(true);
				horizontalBox.setVisible(false);
				remove(title);
				remove(playerName);
				remove(playerPoints);
				
				GUI.player.resetPoints();
				GUI.ans.resetCounter();
				
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
				cardLayout.show(cardDeck, "2"); 			
		}});
		
		answers.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
				cardLayout.next(cardDeck);
		}});
	
		horizontalBox.add(Box.createGlue());
		horizontalBox.add(Box.createGlue());
		horizontalBox.add(startMenu);
		horizontalBox.add(Box.createGlue());
		horizontalBox.add(answers);
		horizontalBox.add(Box.createGlue());
		horizontalBox.add(Box.createGlue());
		
		add(textOne);
		add(textTwo);
		add(showResults);
	
	}
	
	/* Tervehdyksen määritys */
	public String greeting(){
		
		String greeting = "";
		
		if (GUI.player.getPoints() == 0){
			greeting = "Voi ei";
		} else if (GUI.player.getPoints() >= 1 && GUI.player.getPoints() <= 3 ){
			greeting = "Parannettavaa on";
		} else if (GUI.player.getPoints() >= 4 && GUI.player.getPoints() <= 6){
			greeting = "Kohtalaisesti meni";
		} else if (GUI.player.getPoints() >= 7 && GUI.player.getPoints() <= 8){
			greeting = "Hyvä";
		} else if (GUI.player.getPoints() == 9){
			greeting = "Hienoa";
		} else {
			greeting = "Erinomaista";
		}
		
		return greeting;
	}
}
