/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;
import java.awt.*;          
import java.awt.event.*;   

public class StartCard extends JPanel {

	private JPanel cardDeck;
	private JButton start;
	private JButton help;
	private JButton exit;
	private JLabel gameName;
	private JLabel subtitle;
	
	public StartCard (JPanel panel){
	
		/* Aloituskortin määrittely */
		cardDeck = panel;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(Color.lightGray);
		setPreferredSize(new Dimension(500,500));

		/* Otsikko */
		gameName = new JLabel("Oulu-visa");
		gameName.setAlignmentX(Component.CENTER_ALIGNMENT);
		gameName.setFont(new Font("Bookman Old Style", Font.BOLD, 50));
	
		/* Alaotsikko */
		subtitle = new JLabel("Ookko nää Oulusta?");
		subtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		subtitle.setFont(new Font("Bookman Old Style", Font.BOLD, 20));
		subtitle.setForeground(Color.RED);
	
		/* Aloitus */
		start = new JButton("Aloita");
		start.setMaximumSize(new Dimension(200,60));
		start.setFont(new Font("Bookman Old Style", Font.BOLD, 30));
		start.setAlignmentX(Component.CENTER_ALIGNMENT);
		start.addActionListener(new ActionListener(){
		
			public void actionPerformed(ActionEvent e){

				JTextField nameField = new JTextField("Pelaaja1");
		    
				int choice = JOptionPane.showOptionDialog(null, new Object[]{"Anna nimi:", nameField}, "Nimi",
					     	 JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null , new Object[]{"OK", "Peruuta"},null);
	
							 	if (choice == JOptionPane.OK_OPTION) {
							 		
							 		GUI.player.setName(nameField.getText());
							 		CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
							 		cardLayout.next(cardDeck);
							 	}	
		}});
		
		/* Ohje */
		help = new JButton("Ohje");
		help.setMaximumSize(new Dimension(200,60));
		help.setFont(new Font("Bookman Old Style", Font.BOLD, 30));
		help.setAlignmentX(Component.CENTER_ALIGNMENT);
		help.addActionListener(new ActionListener(){
		
			public void actionPerformed(ActionEvent e){
			
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
				cardLayout.show(cardDeck, "1");
			}
		});
	
		/* Lopetus */
		exit = new JButton("Lopeta");
		exit.setMaximumSize(new Dimension(200,60));
		exit.setFont(new Font("Bookman Old Style", Font.BOLD, 30));
		exit.setAlignmentX(Component.CENTER_ALIGNMENT);
		exit.addActionListener(new ActionListener(){
		
			public void actionPerformed(ActionEvent e){
			
				int valinta = JOptionPane.showOptionDialog(null, "Haluatko varmasti lopettaa ohjelman?", "Ohjelman lopettaminen", 
						      JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, new String[]{"Kyllä", "Ei"}, "default");
				
					if (valinta == JOptionPane.YES_OPTION){
				
						System.exit(0);
					}
		}});
	
		add(Box.createRigidArea(new Dimension(0 ,30)));
		add(gameName);
		add(subtitle);
		add(Box.createRigidArea(new Dimension(0 ,30)));
		add(start);
		add(Box.createRigidArea(new Dimension(0,30)));
		add(help);
		add(Box.createRigidArea(new Dimension(0,30)));
		add(exit);
	}
}
