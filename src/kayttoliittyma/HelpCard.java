/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;

import java.awt.*;          
import java.awt.event.*;    

public class HelpCard extends JPanel{

	private JPanel cardDeck;
	private JButton previous;
	private JLabel title;
	private JLabel lineOne, lineTwo, lineThree, lineFour, lineFive, lineSix, lineSeven;
	
	public HelpCard (JPanel panel){
		
		/* Ohjekortin määrittely */
		cardDeck = panel;
		setBackground(Color.lightGray);
		setPreferredSize(new Dimension(500,500));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		/* Otsikko */
		title = new JLabel("Ohje");
		title.setFont(new Font("Bookman Old Style", Font.BOLD, 25));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		title.setBorder(BorderFactory.createEmptyBorder(5 ,0,20 ,0));
		
		/* Ohje teksti */
		lineOne = new JLabel("Oulu-visa on tietovisailu, jossa sinun tulee vastata");
		lineTwo = new JLabel("kymmeneen Oulun kaupunkiin liittyvään kysymykseen.");
		lineThree = new JLabel("Kuhunkin kysymykseen on annettu kolme vastausvaihto-");
		lineFour = new JLabel("ehtoa, joista sinun tulisi loytää se oikea. Vastaamalla");
		lineFive = new JLabel("esitettyihin kysymyksiin, saat selville millainen");
		lineSix = new JLabel("Oulu-tietäjä juuri sinä olet.");
		lineSeven = new JLabel("ONNEA PELIIN!");
		
		lineOne.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineOne.setFont(new Font("Arial", Font.PLAIN, 18));
		lineTwo.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineTwo.setFont(new Font("Arial", Font.PLAIN, 18));
		lineThree.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineThree.setFont(new Font("Arial", Font.PLAIN, 18));
		lineFour.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineFour.setFont(new Font("Arial", Font.PLAIN, 18));
		lineFive.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineFive.setFont(new Font("Arial", Font.PLAIN, 18));
		lineSix.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineSix.setFont(new Font("Arial", Font.PLAIN, 18));
		lineSix.setBorder(BorderFactory.createEmptyBorder(0 ,0,10 ,0));
		lineSeven.setAlignmentX(Component.CENTER_ALIGNMENT);
		lineSeven.setFont(new Font("Arial", Font.BOLD, 18));
		lineSeven.setBorder(BorderFactory.createEmptyBorder(0 ,0,190 ,0));
		
		/* Takaisin */
		previous = new JButton("Takaisin");
		previous.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		previous.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
                cardLayout.show(cardDeck, "2");
			}
		});
		
		add(title);
		
		add(lineOne);
		add(lineTwo);
		add(lineThree);
		add(lineFour);
		add(lineFive);
		add(lineSix);
		add(lineSeven);
		
		add(previous);
	}
}
	
