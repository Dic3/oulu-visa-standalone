/* 
 * Käyttöliittmäohjelmointi - Harjoitustyö
 * Tekijä: Aatos Lang
 */

package kayttoliittyma;

public class Player {
	
	private String name;
	private int points = 0;
	
	/* Setteri nimelle */
	public void setName(String n)
	{
		name = n;
	}
	
	/* Nimen resetointi */
	public void resetName()
	{
		name = null;
	}
	
	/* Getteri nimelle */
	public String getName()
	{
		return name;
	}
	
	/* Pisteiden lisäys */
	public void addPoint()
	{
		points++;
	}
	
	/* Getteri pisteille */
	public int getPoints()
	{
		return points;
	}
	
	/* Pisteiden resetointi */
	public void resetPoints()
	{
		points = 0;
	}
}
