/* 
 * K�ytt�liittm�ohjelmointi - Harjoitusty�
 * Tekij�: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;       
import java.awt.*;          
import java.awt.event.*; 

public class GUI extends JFrame {

	private JPanel cardDeck;
	private HelpCard help;
	private StartCard start;
	private QuestionCard questionOne, questionTwo, questionThree, questionFour, questionFive,
						 questionSix, questionSeven, questionEight, questionNine, questionTen;
	private ResultCard results;
	private AnswerCard answers;
	public static Player player = new Player();
	public static Answers ans = new Answers();
	private CardLayout clayout;
	
	public GUI(){
	
		/* Ikkunan m��rrittely */
		setTitle("Oulu-visa");
		setSize(500, 500);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/* CardLayout */
		cardDeck = new JPanel();
		clayout = new CardLayout();
		cardDeck.setLayout(clayout);

		/* Korttien luominen ja lis��minen */
		createCards();
		addCards();
		
		setContentPane(cardDeck);
		
		/* Menupalkin ja valikkojen m��rittely */
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		/* Tiedosto-valikko */
		JMenu file = new JMenu("Tiedosto");
		JMenuItem newGame = new JMenuItem("Uusi peli"); // Uusi peli
		JMenuItem exit = new JMenuItem("Lopeta"); // Lopetus
		
		/* Ohje-valikko */
		JMenu info  = new JMenu("?");
		JMenuItem infoHelp = new JMenuItem("Ohje"); // Ohjeet
		JMenuItem about = new JMenuItem ("Tietoja ohjelmasta"); // Tietoja
		
		/* ActionListenerit */
		newGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		
				player.resetPoints();
				player.resetName();
				ans.resetCounter();
				cardDeck.removeAll();
				createCards();
				addCards();
		}});
		
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				int choice = JOptionPane.showOptionDialog(null, "Haluatko varmasti lopettaa ohjelman?", "Ohjelman lopettaminen", 
							 JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, new String[]{"Kyll�", "Ei"},
							 "default");
				
							 if (choice == JOptionPane.YES_OPTION)
							 {
								 System.exit(0);
							 }
		}});
		

		about.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Oulu-visa\n\n"
													+ "Tekij�:   Aatos Lang\n"
													+ "Versio:  1.0\n\n",
													"Tietoja ohjelmasta", 
													JOptionPane.PLAIN_MESSAGE);
		}});
		
		infoHelp.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "ALOITUS:\n"
													+ "Pelin aloittaminen tapahtuu klikkaamalla alkuvalikon\n"
													+ "Aloitus-painiketta ja sy�tt�m�ll� pelaajan nimi.\n"
													+ "\nPALUU ALKUVALIKKOON:\n"
													+ "Pelaaja voi palata alkuvalikkoon miss� vaiheessa peli�\n"
													+ "tahansa, valitsemalla yl�palkin vasemmasta reunasta\n"
													+ "Tiedosto -> Uusi peli.\n"
													+ "\nKYSYMYKSET:\n"
													+ "Peliss� pelaajalle esitet��n kymmenen kysymyst�, joihin\n"
													+ "kuhunkin on annettu kolme vastausvaihtoehtoa.\n"
													+ "Vastausvaihtoehdoista pelaaja valitsee yhden,\n"
													+ "joka h�nen mielest��n on oikea vastaus esitettyyn\n"
													+ "kysymykseen.\n"
													+ "\nOHJEET:\n"
													+ "Ohjeistusta peliin l�ytyy alkuvalikon Ohje-painiketta\n"
													+ "klikkaamalla, sek� valitsemalla yl�palkin vasemmasta\n"
													+ "reunasta ? -> Ohje.\n"
													+ "\nTIETOJA OHJELMASTA:\n"
													+ "Tietoja ohjelmasta l�ytyy valitsemalla yl�palkin\n"
													+ "vasemmasta reunasta ? -> Tietoja ohjelmasta.\n"
											 		+ "\nLOPETUS:\n"
											 		+ "Pelaaja voi lopettaa pelin klikkaamalla alkuvalikon\n"
											 		+ "Lopeta-painiketta tai valitsemalla yl�palkin vasemmasta\n"
											 		+ "reunasta Tiedosto -> Lopeta.\n",
											 		"Ohje",
											 		JOptionPane.PLAIN_MESSAGE);
		}});
		
		/* Valikkojen lis�ys */
		file.add(newGame);
		file.addSeparator();
		file.add(exit);
		menuBar.add(file);
		info.add(infoHelp);
		info.addSeparator();
		info.add(about);
		menuBar.add(info);
		
	}
	
	public void createCards(){
		
		start = new StartCard(cardDeck);
		help = new HelpCard(cardDeck);
		results = new ResultCard(cardDeck);
		answers = new AnswerCard(cardDeck);
		
		questionOne = new QuestionCard(cardDeck, "Kysymys 1/10", "Mink� niminen on kuvassa n�kyv� patsas?",
					" a)  Peltipolliisi", " b)  Toripolliisi", " c)  Rautapolliisi",2,
					"src/images/question1.png");
		
		
		questionTwo = new QuestionCard(cardDeck, "Kysymys 2/10", "Mik� kuvan vaakunoista on Oulun kaupungin?",
					"a) 1.", "b) 2.", "c) 3.",1,
					"src/images/question2.png");
		
		
		questionThree = new QuestionCard(cardDeck, "Kysymys 3/10", "Min� vuonna kuvassa n�kyv� Oulun tuomiokirkko on rakennettu?",
					  "a) vuonna 1695", "b) vuonna 1777", "c) vuonna 1793",2,
					  "src/images/question3.png");
		
		
		questionFour = new QuestionCard(cardDeck, "Kysymys 4/10", "Mink� niminen on kauppuri 5:sen legendaarinen hampurilainen?",
					 "a) Ratsastaja", "b) Katsastaja", "c) Teurastaja",3,
				 	 "src/images/question4.png");
		
		
		questionFive = new QuestionCard(cardDeck, "Kysymys 5/10", "Ket� kuvassa n�kyv� Oulun vanhin julkinen ulkoveistos esitt��?",
					 "a) Frans Mikael Franz�nia", "b) Eino Leinoa", "c) Leevi Madetojaa",1,
					 "src/images/question5.png");
		
		questionSix = new QuestionCard(cardDeck, "Kysymys 6/10", "Min� vuonna kuvassa n�kyv� Oulun kaupungintalo valmistui?",
					"a) vuonna 1886", "b) vuonna 1750", "c) vuonna 1901",1,
					"src/images/question6.png");
		
		questionSeven = new QuestionCard(cardDeck, "Kysymys 7/10", "Mik� Oulun seudun lukio kuvassa on?",
					  "a) Madetojan musiikkilukio", "b) Oulun Lyseon lukio", "c) Oulun Suomalaisen Yhteiskoulun lukio",3,
					  "src/images/question7.png");
		
		questionEight = new QuestionCard(cardDeck, "Kysymys 8/10", "Min� vuonna kuvassa n�kyv� Oulun kauppahalli on rakennettu?",
					  "a) vuonna 1850", "b) vuonna 1901", "c) vuonna 1920",2,
					  "src/images/question8.png");
		
		questionNine = new QuestionCard(cardDeck, "Kysymys 9/10", "Min� vuonna Oulun yliopisto on perustettu?",
					 "a) vuonna 1920", "b) vuonna 1958", "c) vuonna 1973",2,
					 "src/images/question9.png");
		
		questionTen = new QuestionCard(cardDeck, "Kysymys 10/10", "Mill� tutummalla nimell� pizza tunnetaan Oulussa?",
					"a) kannku", "b) rieska", "c) k�nkky",3,
					"src/images/question10.png");
		}
	
	public void addCards(){
		
		cardDeck.add(help, "1");
		cardDeck.add(start, "2");
		cardDeck.add(questionOne, "3");
		cardDeck.add(questionTwo, "4");
		cardDeck.add(questionThree, "5");
		cardDeck.add(questionFour, "6");
		cardDeck.add(questionFive, "7");
		cardDeck.add(questionSix, "8");
		cardDeck.add(questionSeven, "9");
		cardDeck.add(questionEight, "10");
		cardDeck.add(questionNine, "11");
		cardDeck.add(questionTen, "12");
		cardDeck.add(results, "13");
		cardDeck.add(answers, "14");
		
		clayout.show(cardDeck, "2");
	}
}
