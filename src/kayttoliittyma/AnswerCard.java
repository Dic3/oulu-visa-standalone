/* 
 * K�ytt�liittm�ohjelmointi - Harjoitusty�
 * Tekij�: Aatos Lang
 */

package kayttoliittyma;

import javax.swing.*;
import java.awt.*;          
import java.awt.event.*;    

public class AnswerCard extends JPanel{

	private JPanel cardDeck;
	private JPanel buttons;
	private JPanel answer;
	private JPanel text;
	private JPanel back;
	private JButton previous;
	private JButton buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive,
					buttonSix, buttonSeven, buttonEight, buttonNine, buttonTen;
	private JLabel helpText;
	private JLabel questionText;
	private JLabel question;
	private JLabel optionOne, optionTwo, optionThree;
	private JLabel rightAnswer;
	private JLabel playersAnswer;
	private JLabel title;
	private ImageIcon right;
	private ImageIcon wrong;
	private ImageIcon x;
	private JLabel frame;
	private String rightAns;
	
	
	public AnswerCard (JPanel panel){
		
		/* Vastauskortin m��rittely */
		cardDeck = panel;
		setBackground(Color.lightGray);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(500,500));
		
		previous = new JButton("Takaisin");
		
		buttonOne = new JButton("1");
		buttonTwo = new JButton("2");
		buttonThree = new JButton("3");
		buttonFour = new JButton("4");
		buttonFive = new JButton("5");
		buttonSix = new JButton("6");
		buttonSeven = new JButton("7");
		buttonEight = new JButton("8");
		buttonNine = new JButton("9");
		buttonTen = new JButton("10");
		
		buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		buttons.setBackground(Color.lightGray);
		buttons.setAlignmentX(Component.CENTER_ALIGNMENT);
			
		answer = new JPanel();
		answer.setLayout(new BoxLayout(answer, BoxLayout.Y_AXIS));
		answer.setBorder(BorderFactory.createEmptyBorder(0,100,250,100));
		answer.setPreferredSize(new Dimension(200, 200));
		answer.setBackground(Color.lightGray);
		answer.setAlignmentX(Component.CENTER_ALIGNMENT);
			
		text = new JPanel();
		text.setLayout(new BoxLayout(text, BoxLayout.Y_AXIS));
		text.setBackground(Color.lightGray);
		text.setAlignmentX(Component.CENTER_ALIGNMENT);

		title = new JLabel("Vastaukset");
		title.setFont(new Font("Bookman Old Style", Font.BOLD, 25));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		title.setBorder(BorderFactory.createEmptyBorder(5 ,0,10 ,0));
		text.add(title);
		
		helpText = new JLabel("Tarkastele vastauksia klikkaamalla haluamaasi kysymyst�.");
		helpText.setFont(new Font("Arial", Font.PLAIN, 18));
		helpText.setAlignmentX(Component.CENTER_ALIGNMENT);
		helpText.setBorder(BorderFactory.createEmptyBorder(0 ,0,10 ,0));
		text.add(helpText);
			
		questionText = new JLabel("Kysymykset:");
		questionText.setFont(new Font("Arial", Font.PLAIN, 18));
		questionText.setAlignmentX(Component.CENTER_ALIGNMENT);
		text.add(questionText);
		
		back = new JPanel();
		back.setBorder(BorderFactory.createEmptyBorder(60 ,0,0,0));
		back.setBackground(Color.lightGray);
		back.setAlignmentX(Component.CENTER_ALIGNMENT);
		back.add(previous);
		
		right = new ImageIcon("src/Images/right.png");
		wrong = new ImageIcon("src/Images/wrong.png");
	
		/* ActionListenerit */
		previous.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				answer.removeAll();
				answer.setBackground(null);
			
				CardLayout cardLayout = (CardLayout) cardDeck.getLayout();
				cardLayout.show(cardDeck, "13");
	    }});
		
		buttonOne.addActionListener(new ActionListener(){
		
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "b";
		
				question = new JLabel("Kysymys 1/10");
				optionOne = new JLabel("a) Peltipoliisi");
				optionTwo = new JLabel("b) Toripolliisi");
				optionThree = new JLabel("c) Rautapoliisi");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsOne());
				
				if (GUI.ans.getAnsOne().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonTwo.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "a";
		
				question = new JLabel("Kysymys 2/10");
				optionOne = new JLabel("a) 1.");
				optionTwo = new JLabel("b) 2.");
				optionThree = new JLabel("c) 3.");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsTwo());
				
				if (GUI.ans.getAnsTwo().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonThree.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "b";
		
				question = new JLabel("Kysymys 3/10");
				optionOne = new JLabel("a) vuonna 1695");
				optionTwo = new JLabel("b) vuonna 1777");
				optionThree = new JLabel("c) vuonna 1793");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " +GUI.ans.getAnsThree());
				
				if (GUI.ans.getAnsThree().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonFour.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "c";
		
				question = new JLabel("Kysymys 4/10");
				optionOne = new JLabel("a) Ratsastaja");
				optionTwo = new JLabel("b) Katsastaja");
				optionThree = new JLabel("c) Teurastaja");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsFour());
				
				if (GUI.ans.getAnsFour().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonFive.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "a";
		
				question = new JLabel("Kysymys 5/10");
				optionOne = new JLabel("a) Frans Mikael Franz�nia");
				optionTwo = new JLabel("b) Eino Leinoa");
				optionThree = new JLabel("c) Leevi Madetojaa");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsFive());
				
				if (GUI.ans.getAnsFive().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonSix.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "a";
		
				question = new JLabel("Kysymys 6/10");
				optionOne = new JLabel("a) vuonna 1886");
				optionTwo = new JLabel("b) vuonna 1750");
				optionThree = new JLabel("c) vuonna 1901");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsSix());
				
				if (GUI.ans.getAnsSix().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonSeven.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "c";
		
				question = new JLabel("Kysymys 7/10");
				optionOne = new JLabel("a) Madetojan musiikkilukio");
				optionTwo = new JLabel("b) Oulun Lyseon lukio");
				optionThree = new JLabel("c) Oulun Suomalaisen Yhteiskoulun lukio");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsSeven());
				
				if (GUI.ans.getAnsSeven().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonEight.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "b";
		
				question = new JLabel("Kysymys 8/10");
				optionOne = new JLabel("a) vuonna 1850");
				optionTwo = new JLabel("b) vuonna 1901");
				optionThree = new JLabel("c) vuonna 1920");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsEight());
				
				if (GUI.ans.getAnsEight().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonNine.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "b";
		
				question = new JLabel("Kysymys 9/10");
				optionOne = new JLabel("a) vuonna 1920");
				optionTwo = new JLabel("b) vuonna 1958");
				optionThree = new JLabel("c) vuonna 1973");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsNine());
				
				if (GUI.ans.getAnsNine().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
		
		buttonTen.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
		
				answer.removeAll();
				answer.setBackground(Color.GRAY);
			
				rightAns = "c";
		
				question = new JLabel("Kysymys 10/10");
				optionOne = new JLabel("a) kankku");
				optionTwo = new JLabel("b) rieska");
				optionThree = new JLabel("c) k�nkky");
				rightAnswer = new JLabel ("Oikea vastaus: " + rightAns);
				playersAnswer = new JLabel("Sinun vastaus: " + GUI.ans.getAnsTen());
				
				if (GUI.ans.getAnsTen().equals(rightAns)){
					answer.setBackground(Color.green);
					x = right;
				} else {
					answer.setBackground(Color.red);
					x = wrong;
				}
					
				frame = new JLabel(x);
				frame.setBorder(BorderFactory.createEmptyBorder(0 ,60,10 ,0));
					
				answer.add(question);
				answer.add(optionOne);
				answer.add(optionTwo);
				answer.add(optionThree);
				answer.add(rightAnswer);
				answer.add(playersAnswer);
				answer.add(frame);
				answer.validate();
				validate();
		}});
	
		buttons.add(buttonOne);
		buttons.add(buttonTwo);
		buttons.add(buttonThree);
		buttons.add(buttonFour);
		buttons.add(buttonFive);
		buttons.add(buttonSix);
		buttons.add(buttonSeven);
		buttons.add(buttonEight);
		buttons.add(buttonNine);
		buttons.add(buttonTen);
		
		add(text);
		add(buttons);
		add(answer);
		add(back);
		
		
	}
}
